The .circ files in Source -> src run on the program Logisim 2.

To run a program, open up run.circ and modify the contents of the instruction memory unit. Optionally, you can load a .hex file (the machine code) into the instruction memory.

Original Project description at https://inst.eecs.berkeley.edu/~cs61c/sp16/projs/04/. This project was from the CS61C course at Berkeley.